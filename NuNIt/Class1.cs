﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using CILearning;

namespace NuNIt
{
    [TestFixture]
    public class Class1
    {
        [Test]
        public void ToDo_ArgIs12_Returne12()
        {
            //Arrange
            ToDoClass instanceOfToDoClass = new ToDoClass();
            int arg = 12;
            int expectedResult = 12;

            //Act
            int actualResult = instanceOfToDoClass.ToDo(arg);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void ToDo_ArgIs2_Returne0()
        {
            //Arrange
            ToDoClass instanceOfToDoClass = new ToDoClass();
            int arg = 2;
            int t = 9;
            int expectedResult = 0;

            //Act
            int actualResult = instanceOfToDoClass.ToDo(arg);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
